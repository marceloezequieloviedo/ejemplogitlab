#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include<stdbool.h>

//valor de control
int CONT_comieron = 0; 

//defino palillos 
bool p1 = true; //todos los palillos empiezan estando en la disponibles.
bool p2 = true; //true = el palillo esta sobre la mesa.
bool p3 = true; //false = el palillo esta en las manos de algun filosofo.
bool p4 = true;
bool p5 = true;

//defino los mutex
pthread_mutex_t palillo1;
pthread_mutex_t palillo2;
pthread_mutex_t palillo3;
pthread_mutex_t palillo4;
pthread_mutex_t palillo5;

void do_nothing(){
  usleep(200000); // delay
}

void accion(void* arg){
	char *id;
	id = (char *)arg;
	
/**/	int i = 1; //define los ciclos y cuantas veces va a comer cada filosofo
	while (i<6) {
	  //Estado Pensando
	  do_nothing();
	  printf("Soy el Filósofo %s : Pensando\n",id);
	  	  
	  //Estado agarrando palillos
/**/	  if(id == "1"){ 
	  	do_nothing();
	  	pthread_mutex_lock(&palillo2); p2 = false;
	  	printf("Soy el Filósofo %s : Agarrando palillo izquierdo\n",id);
	  	
	  	if(p1){ //Si palillo1 esta disponible lo agarra..
	  	 do_nothing(); 
	  	 pthread_mutex_lock(&palillo1); p1 = false;
	  	 printf("Soy el Filósofo %s : Agarrando palillo derecho\n",id);
	  	 //bloqueo la seccion critica que altera el estado del palillo 
	  	 //y enuncia que tomo el palillo
	  	
	  	 //Estado comiendo...
	  	 do_nothing(); do_nothing(); //
	  	 printf("Soy el Filósofo %s : Comiendo (%i)\n",id,i); i++;
	  	 CONT_comieron++; //valor de control
	  	 p2 = true ;pthread_mutex_unlock(&palillo2);
	  	 p1 = true; pthread_mutex_unlock(&palillo1); 
	  	 // devuelve lo palillos y desbloquea la seccion critica
	  	 	  	 
	  	}else{ //..sino suelta pallillo2 sin haber comido
	  	 pthread_mutex_unlock(&palillo2); p2 = true; 
	  	}
	  	
	  } else if(id == "2"){
	  	do_nothing();
	  	pthread_mutex_lock(&palillo1); p1 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo izquierdo\n",id);
	  	
	  	if(!p1 && !p5){ p1 = true; pthread_mutex_unlock(&palillo1);}
	  	//para evitar el deadlock
	  	
	  	do_nothing();
	  	pthread_mutex_lock(&palillo5); p5 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo derecho\n",id);
	  	
	  	//Estado comiendo...
	  	do_nothing(); do_nothing();
	  	printf("Soy el Filósofo %s : Comiendo (%i)\n",id,i); i++;
	  	CONT_comieron++; //valor de control
	  	p1 = true; pthread_mutex_unlock(&palillo1);
	  	p5 = true; pthread_mutex_unlock(&palillo5); 
	  	
	  	  
	  } else if(id == "3"){
	  	do_nothing();
	  	pthread_mutex_lock(&palillo5); p5 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo izquierdo\n",id);
	  	
	  	if(!p1 && !p4){ p1 = true; pthread_mutex_unlock(&palillo1);}
	  	//para evitar el deadlock
	  	
	  	do_nothing();
	  	pthread_mutex_lock(&palillo4); p4 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo derecho\n",id);
	  	
	  	//Estado comiendo...
	  	do_nothing(); do_nothing();
	  	printf("Soy el Filósofo %s : Comiendo (%i)\n",id,i); i++;
	  	CONT_comieron++; //valor de control
	  	p5 = true; pthread_mutex_unlock(&palillo5); 
	  	p4 = true; pthread_mutex_unlock(&palillo4); 
	  	
	  		  
	  } else if(id == "4"){
	  	do_nothing();
	  	pthread_mutex_lock(&palillo4); p4 = false; 
		printf("Soy el Filósofo %s : Agarrando palillo izquierdo\n",id);
	  	
	  	if(!p1 && !p3){ p1 = true; pthread_mutex_unlock(&palillo1);}
	  	//para evitar el deadlock
	  	
	  	do_nothing();
	  	pthread_mutex_lock(&palillo3); p3 = false;
	  	printf("Soy el Filósofo %s : Agarrando palillo derecho\n",id);
	  	
	  	//Estado comiendo...
	  	do_nothing(); do_nothing();
	  	printf("Soy el Filósofo %s : Comiendo (%i)\n",id,i); i++;
	  	CONT_comieron++; //valor de control
	  	p4 = true; pthread_mutex_unlock(&palillo4); 
	  	p3 = true; pthread_mutex_unlock(&palillo3);
	  	  
	  		
	  } else if(id == "5"){
	  	do_nothing();
	  	pthread_mutex_lock(&palillo3); p3 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo izquierdo\n",id);
	  	
	  	if(!p1 && !p2){ p1 = true; pthread_mutex_unlock(&palillo1);}
	  	//para evitar el deadlock
	  	
	  	do_nothing();
	  	pthread_mutex_lock(&palillo2); p2 = false; 
	  	printf("Soy el Filósofo %s : Agarrando palillo derecho\n",id);
	  	
	  	//Estado comiendo...
	  	do_nothing(); do_nothing();
	  	printf("Soy el Filósofo %s : Comiendo (%i)\n",id,i); i++;
	  	CONT_comieron++; //valor de control
	  	p3 = true; pthread_mutex_unlock(&palillo3); 
	  	p2 = true; pthread_mutex_unlock(&palillo2);
	  	
	  	
	  }
	  do_nothing();
/*	  i++;*/
	}	
}

////////////////////////

int main(int argc, char *argv[])
{	
	//defino los filosofos
	char* uno = "1"; 
	char* dos = "2"; 
	char* tres = "3";
	char* cuatro = "4"; 
	char* cinco = "5"; 
	// inicializo los mutex
  	pthread_mutex_init(&palillo1,NULL);
  	pthread_mutex_init(&palillo2,NULL);
  	pthread_mutex_init(&palillo3,NULL);
  	pthread_mutex_init(&palillo4,NULL);
  	pthread_mutex_init(&palillo5,NULL);
  	//hilos
	pthread_t filo1;
	pthread_t filo2;
	pthread_t filo3;
	pthread_t filo4;
	pthread_t filo5;
 	pthread_create(&filo1,NULL,(void*) accion,(void*) uno);
 	pthread_create(&filo2,NULL,(void*) accion,(void*) dos);
 	pthread_create(&filo3,NULL,(void*) accion,(void*) tres);
 	pthread_create(&filo4,NULL,(void*) accion,(void*) cuatro);
 	pthread_create(&filo5,NULL,(void*) accion,(void*) cinco);
 	pthread_join(filo1,NULL);
 	pthread_join(filo2,NULL);	
 	pthread_join(filo3,NULL);
 	pthread_join(filo4,NULL);	
 	pthread_join(filo5,NULL);
 		
	//VALOR DE CONTROL...
	printf("Comieron %i veces entre todos los filosofos\n", CONT_comieron);	
	
	return EXIT_SUCCESS;
}
