#include <semaphore.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define CANT_PRINTS 1

sem_t s1;
sem_t s2;
sem_t s3;
sem_t s4;
sem_t s5;
sem_t s6;
sem_t s7;
int t = 200000;
void* msjPienso()   // 1
{

 int i;
 for(i=0; i < CANT_PRINTS; i++ ){
    //sem_wait(&s1);
    usleep(t);  //*
    printf("1 pienso\n"); 
 }
 sem_post(&s2);
 sem_post(&s3);
 sem_post(&s4);
 sem_post(&s5);
 pthread_exit(NULL);
}

void* msjMientrasLavo () // 2
{
 
 int i;
 for(i=0; i < CANT_PRINTS-1; i++ ){   //le quito una impresion del for
    usleep(t);  //*
    printf("2 Mientras lavo los platos\n"); 
   }
 sem_wait(&s2);
 usleep(t);  //*
 printf("2 Mientras lavo los platos\n"); //la vuelvo a agregar para que tras el semaforo termine su ejecucion;
 pthread_exit(NULL);
} 

void* msjMientrasLimpio()  // 3
{
 int i;
 for(i=0; i < CANT_PRINTS-1; i++ ){ //*
    usleep(t);  //*
    printf("3 Mientras limpio el piso\n");
   }
 sem_wait(&s3); 
 usleep(t);  //*
 printf("3 Mientras limpio el piso\n"); //*
 pthread_exit(NULL); 
} 

void* msjMientrasRiego()   // 4
{
 int i;
 for(i=0; i < CANT_PRINTS-1; i++ ){ //*
    usleep(t);  //*
    printf("4 Mientras riego las plantas\n");
   }
 sem_wait(&s4); 
 usleep(t);  //*
 printf("4 Mientras riego las plantas\n"); //*
 pthread_exit(NULL); 
} 

void* msjExisto()  // 5
{
 sem_wait(&s5);
 int i;
 for(i=0; i < CANT_PRINTS; i++ ){
    usleep(t);  //*
    printf("5 Existo!!!\n");
 }
 sem_post(&s6);
 sem_post(&s7);
 pthread_exit(NULL); 
} 

void* msjHablar ()  // 6
{
 sem_wait(&s6);
 int i;
 for(i=0; i < CANT_PRINTS; i++ ){
    usleep(t);  //*
    printf("6 Hablar...\n");
   }
 pthread_exit(NULL); 
} 

void* msjDecision()  // 7
{
 sem_wait(&s7);
 int i;
 for(i=0; i < CANT_PRINTS; i++ ){
    usleep(t);  //*
    printf("7 Tomar una decision\n");
   }
 pthread_exit(NULL); 
} 

int main()  // main
{
 pthread_t t1;
 pthread_t t2;
 pthread_t t3;
 pthread_t t4;
 pthread_t t5;
 pthread_t t6;
 pthread_t t7;
//INICIALIZO LOS SEMAFOROS

 int res= sem_init(&s1,0,1);
 res=sem_init(&s2,0,0);
 res=sem_init(&s3,0,0);
 res=sem_init(&s4,0,0);
 res=sem_init(&s5,0,0);
 res=sem_init(&s6,0,0);
 res=sem_init(&s7,0,0);

//INICIALIZO LOS THREADS

 int rc= pthread_create(&t1,NULL,msjPienso,NULL);
 rc=pthread_create(&t2,NULL,msjMientrasLavo,NULL);
 rc=pthread_create(&t3,NULL,msjMientrasLimpio,NULL);
 rc=pthread_create(&t4,NULL,msjMientrasRiego,NULL);
 rc=pthread_create(&t5,NULL,msjExisto,NULL);
 rc=pthread_create(&t6,NULL,msjHablar,NULL);
 rc=pthread_create(&t7,NULL,msjDecision,NULL);
//DEJO QUE LOS THREAD TERMINEN
 pthread_join(t1,NULL);
 pthread_join(t2,NULL);
 pthread_join(t3,NULL);
 pthread_join(t4,NULL);
 pthread_join(t5,NULL);
 pthread_join(t6,NULL);
 pthread_join(t7,NULL);
//DESTRUYO LOS SEMAFOROS
 sem_destroy(&s1);
 sem_destroy(&s2);
 sem_destroy(&s3);
 sem_destroy(&s4);
 sem_destroy(&s5);
 sem_destroy(&s6);
 sem_destroy(&s7);

pthread_exit(NULL);

}

// :D

